class UsersController < ApplicationController
  
  def show
  	@user = User.find_by_id(params[:id])
        @title = @user.name 
  end
  
  def new
    @user = User.new
    @title = "Sign Up"
  end
  
  def create
    #raise params[:user].inspect
    @user = User.new(params[:user])
    if @user.save
      #Handle the successful save
      redirect_to user_path(@user), flash: {success: "Welcome to Sample App! - A micro Twitter attempt"}
    else
      @title = "Sign Up"
      render 'new'
    end
  end

end
