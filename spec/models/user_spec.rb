# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  email              :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  encrypted_password :string(255)
#  salt               :string(255)
#


require 'spec_helper'

describe User do

  before(:each) do
    @attr = { name:"someone", 
              email:"someone@example.com", 
              password:"foobar", 
              password_confirmation:"foobar"}
  end
 
  it "should create a new instance given valid attributes" do
    User.create!(@attr)
  end

  it "should require a name" do
    no_name_user = User.new(@attr.merge(name: ""))
    no_name_user.should_not be_valid
  end
  
  it "should reject names  that are too long" do
    long_name = "a"*51
    long_name_user = User.new(@attr.merge(name: long_name))
    long_name_user.should_not be_valid
  end 
  
  it "should reject invalid usernames" do
    usernames = %w[ '       ' *jhjs)* :"{nnnnnnnnnnssssss  .hela ]
    usernames.each do |username|
      invalid_username = User.new(@attr.merge(name: username))
      invalid_username.should_not be_valid 
    end
  end

  it "should accept valid usernames" do
    usernames = %w[ user123 userabdc user_name_test  user_ ]
    usernames.each do |username|
      invalid_username = User.new(@attr.merge(name: username))
      invalid_username.should be_valid 
    end
  end

  it "should require an email" do
    no_email_user = User.new(@attr.merge(email: ""))
    no_email_user.should_not be_valid
  end

  it "should reject invalid email addresses" do
    addresses = %w[user@foo,com THE_USER@foo. first@last@example.com  user.foo.com]
    addresses.each do |address|
      invalid_email_address = User.new(@attr.merge(email: address))
      invalid_email_address.should_not be_valid 
    end
  end
    
  it "should accept valid email addresses" do
    addresses = %w[user@foo.com THE_USER@foo.bar.com first.last@example.com]
    addresses.each do |address|
      valid_email_address = User.new(@attr.merge(email: address))
      valid_email_address.should be_valid
    end
  end

  #This is to prevent user from entering unwanted and arbitrarily large strings
  it "should reject emails that are too long" do
    long_email = "a"*81
    long_email = long_email + "@example.com"
    long_email_user = User.new(@attr.merge(email: long_email))
    long_email_user.should_not be_valid
  end

  it "should reject duplicate email addresses" do
    User.create(@attr)
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end

  it "should reject email addresses that appear different but are same(i.e check for same email in upper and lower case)" do
    up_case_email = 'USER@EXAMPLE.COM'
    User.create(@attr.merge(email: up_case_email))
    low_case_email = 'user@example.com'
    user_with_duplicate_low_case_email = User.new(@attr.merge(email: low_case_email))
    user_with_duplicate_low_case_email.should_not be_valid
  end
 
  describe "passwords" do

    before(:each) do
      @user = User.new(@attr)
    end
    
    it "should have a password attribute" do
      @user.should respond_to(:password)
    end

    it "should have a password confirmation attribute" do
      @user.should respond_to(:password_confirmation)
    end
  end

  describe "password validations" do
    it "should require a password" do
      User.new(@attr.merge(password: "", password_confirmation: "")).should_not be_valid
    end
    
    it "should require a matching password validation" do
      User.new(@attr.merge(password_confirmation: "invalid"))
    end
    
    it "should reject short password" do
      short="a"*5
      User.new(@attr.merge(password:short, password_confirmation:short)).should_not be_valid
    end
    
    it "should reject very long password" do
      long="a"*81
      User.new(@attr.merge(password:long, password_confirmation:long)).should_not be_valid
    end
  end

  describe "password encryption" do
  
    before(:each) do
      @user = User.create!(@attr)
    end

    it "should have an encrypted password attribute" do
      @user.should respond_to(:encrypted_password)
    end
  
    it "should set the encrypted passsword attribute" do 
      @user.encrypted_password.should_not be_blank
    end

    it "should have a salt" do
      @user.should respond_to(:salt)
    end

  end

  describe "has_password? method" do
    
    before(:each) do
      @user = User.create!(@attr)
    end
  
    it "should exist" do
      @user.should respond_to(:has_password?)
    end

    it "should return true if passwords match" do
      @user.has_password?(@attr[:password]).should be_true
    end

    it "should return false if passwords dont match" do
      @user.has_password?("invalid").should be_false
    end
  
  end

  describe "authenticate method" do
    
    before(:each) do
      @user = User.create!(@attr)
    end
    
    it "should exist" do
      User.should respond_to(:authenticate)
    end
    
    it "should return nil on email password mismatch" do
      User.authenticate(@attr[:email], "invalid_password").should be_nil
    end

    it "should return nil on email address with no user" do
      User.authenticate("wrongemail@id.com", @attr[:password]).should be_nil
    end

    it "should return the user on email and password match" do
      User.authenticate(@attr[:email], @attr[:password]).should == @user
    end


  end

end
