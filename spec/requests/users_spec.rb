require 'spec_helper'

describe "Users" do
  describe "Sign Up" do
    describe "failure" do
      it "should not create a new user" do
        lambda do
          visit signup_path
          fill_in "Name",          with: ""
          fill_in "Email",         with: ""
          fill_in "Password",      with: ""
          fill_in "Confirmation",  with: ""
          click_button
          response.should render_template('users/new')
          response.should have_selector('div#error_explaination')
        end.should_not change(User, :count)
      end
    end
    describe "success" do
      it "should create new user" do
        lambda do
          visit signup_path
          fill_in "Name",         with: "someone"
          fill_in "Email",        with: "someone@somedomain.com"
          fill_in "Password",     with: "secret"
          fill_in "Confirmation", with: "secret"
          click_button
          response.should have_selector('div.flash.success', content: "Welcome to Sample App")
          response.should render_template('users/show')
          response.should render_template('users/new')
        end.should change(User, :count).by(1)
      end
    end
  end
end
