require 'spec_helper'

describe "LayoutLinks" do
#  describe "GET /layout_links" do
#    it "works! (now write some real specs)" do
#      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
#      get layout_links_index_path
#      response.status.should be(200)
#    end

    it "should have a home page at /" do
      get '/'
      response.should have_selector('title', content: "Home")
    end
    
    it "should have a Contact page at /contact" do
      get '/contact'
      response.should have_selector('title', content: "Contact")
    end
    
    it "should have a About page at /contact" do
      get '/about'
      response.should have_selector('title', content: "About")
    end
    
    it "should have a Contact page at /help" do
      get '/help'
      response.should have_selector('title', content: "Help")
    end
    
    it "should have a Contact Sign Up page at /signup" do
      get '/signup'
      response.should have_selector('title', content: "Sign Up")
    end

    it "should have the right links on a layout" do

      visit root_path
      response.should have_selector('a[href="/"]>img')
      response.should have_selector('title', content:"Home")
      click_link "About"
      response.should have_selector('title', content: "About")
      click_link "Contact"
      response.should have_selector('title', content: "Contact")
      click_link "Home"
      response.should have_selector('title', content: "Home")
      click_link "Sign Up Now!"
      response.should have_selector('title', content: "Sign Up")
      click_link "Help"
      response.should have_selector('title', content: "Help")
    end

end
