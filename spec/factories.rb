FactoryGirl.define do
  factory :user do 
    name                   "someone"
    email                  "someone@somedomain.com"
    password               "secret"
    password_confirmation  "secret"
  end
end
